//First we have to create an admin user in the admin database. This user will then have the ability to create other
//users in the system
db.createUser(
  {
    user: adminUser,
    pwd: adminPass,
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)

//Login as the user we just created
db.auth(adminUser, adminPass)
//The database we want to create
db.getSiblingDB(database)
//The user that has permissions to read and write to this database
db.createUser(
  {
    user: dbUser,
    pwd: dbPass,
    roles: [
      { role: "readWrite", db: database }
    ]
  }
)
