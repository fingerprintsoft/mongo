FROM mongo:3.2.6
MAINTAINER fuzail@tessarectit.com

ENV adminUser tesseract
ENV adminPass iamanapplication
ENV database management
ENV dbUser administrator
ENV dbPass iamanapplication

COPY docker-entrypoint.sh /entrypoint.sh
COPY mongod.yml /etc/mongod.conf
COPY seed.js /seed.js

RUN \
  chown -R mongodb:mongodb /etc/mongod.conf;\
  chmod 755 /etc/mongod.conf

EXPOSE 27017
CMD ["mongod","-f", "/etc/mongod.conf"]
